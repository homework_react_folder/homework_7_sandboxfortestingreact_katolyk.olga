import React from "react";
import { render, screen } from "@testing-library/react";
// import { render } from "@testing-library/react";
// import "@testing-library/jest-dom";
import Button from "./Button.jsx";

describe("Button test", () => {
    test("should render the button with text", ()=> {
//         // const button = render(<Button>test</Button>)
//         // expect(button.getByText(/test/i)).toBeIntheDocument;
        render(<Button>test</Button>);
        expect(screen.getByText(/test/i)).toBeInTheDocument();
    });
})
